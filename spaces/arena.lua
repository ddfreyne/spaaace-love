local AnimationSystem    = require('systems/animation')
local CameraSystem       = require('systems/camera')
local HyperjumpSystem    = require('systems/hyperjump')
local InputSystem        = require('systems/input')
local PhysicsSystem      = require('systems/physics')
local RenderingSystem    = require('systems/rendering')
local SteeringSystem     = require('systems/steering')

local Starfield          = require('renderers/starfield')

local Camera             = require('entities/camera')

require('engine/types')



local Arena = {}
Arena.__index = Arena

function Arena.new(world, starSystem)
  local entitiesCollection = EntitiesCollection.new()
  entitiesCollection:add(Camera.new(0, 0))
  entitiesCollection:add(world.ship)
  for _, planet in ipairs(starSystem.planets) do
    entitiesCollection:add(planet)
  end

  local t = {
    world              = world,
    entitiesCollection = entitiesCollection,
    starSystem         = starSystem,

    starfield       = Starfield.new(world.ship, entitiesCollection),

    cameraSystem    = CameraSystem.new(entitiesCollection),
    animationSystem = AnimationSystem.new(entitiesCollection),
    inputSystem     = InputSystem.new(world, entitiesCollection),
    renderingSystem = RenderingSystem.new(entitiesCollection),
    physicsSystem   = PhysicsSystem.new(entitiesCollection),
    steeringSystem  = SteeringSystem.new(entitiesCollection),
    hyperjumpSystem = HyperjumpSystem.new(entitiesCollection),
  }

  return setmetatable(t, Arena)
end

function Arena:update(dt)
  self.inputSystem:update(dt)
  self.physicsSystem:update(dt)
  self.steeringSystem:update(dt)
  self.hyperjumpSystem:update(dt)
  self.animationSystem:update(dt)
  self.cameraSystem:update(dt)
end

function Arena:draw()
  self.starfield:draw(function()
    self.renderingSystem:update(0)
  end)
end

function Arena:keypressed(key)
  self.inputSystem:keypressed(key)
end

return Arena
