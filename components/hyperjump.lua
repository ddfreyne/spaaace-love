local HyperjumpComponent = {}
HyperjumpComponent.__index = HyperjumpComponent

function HyperjumpComponent.new()
  local t = {
    progress    = 0.0,
    destination = nil,
  }

  return setmetatable(t, HyperjumpComponent)
end

return HyperjumpComponent
