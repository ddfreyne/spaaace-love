# Ideas

* **Investing**: Invest in planets and systems to increase available commodities. If the material is already abundant, it will become even cheaper, making it more profitable for trading. If the material is non-existant, it will become much less profitable for trading, but the player will get a nice cash return for investing in the planet anyway.

* **Set up trade routes**: Hire traders to do the trading for you.

* **Illegal goods**: Trading drugs and weapons is not allowed in most systems. Being fined on entering a system (with a certain chance) is a good first implementation. Afterwards, police ships that scan vessels and give you the opportunity to either pay the fine, escape and/or fight.

* **Requesting landing permission**: Landing should not always be permitted. Restricted space stations and hostile planets shouldn’t let you land.

* **Supply/demand based economy**: Planets generate a limited number of resources. These resource availabilities vary over time (think bad harvest). Planets can have facilities for combining one type of resource into other types. Planets also have basic requirements (food, water).

* **Different player types**: trader, courier, bounty hunter, pirate

## Other

* Passage of time
* Player name
* Player credits
* Trading
* Upgrading ship
* Replacing ship

## Prettiness

* Add circular landing animation

## Small things

* Limit hyperspace range based on hyperdrive power in ship and amount of fuel
* Require fuel for hyperdrive
* Add credits screen
* Add options screen
* Help menu

## Gameplay progression

Gameplay progression could be something like this:

1. Trading in same system
2. Trading with other systems
3. Upgrading spaceship
4. Investing in systems
5. Exploring and finding rare commodities

## Trading

Income:

* Mining
* Trading (selling at higher price)

Expense:

* Better outfit, better ships
	- Existing ship has partial trade-in value
* Fuel (for hyperspace jumps only?)
* Ammunition
* Trading fees
* Ship repair fees

Balancing:

* Commodity dependency tree: item prices depend on ingredients

…

## Ship AI

* Ships entering hyperspace
* Ships coming from hyperspace
* Ships that land
* Ships that follow you for a while (police/miltary)

## Development tools

* Pause button, step button
* Inspector
