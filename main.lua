require('engine/types')

local Gamestate = require('vendor/hump/gamestate')

local MainState = require('gamestates/main')
local MenuState = require('gamestates/menu')
local World     = require('entities/world')

require('vendor/LoveFrames')
loveframes.util.SetActiveSkin('Spaaace')

local world

math.randomseed(os.time())

function love.load()
  mainState = MenuState.new()

  Gamestate.registerEvents()
  Gamestate.switch(mainState)
end
