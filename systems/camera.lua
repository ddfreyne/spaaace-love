local CameraSystem = {}
CameraSystem.__index = CameraSystem

function CameraSystem.new(entitiesCollection)
  local t = {
    entitiesCollection = entitiesCollection,
  }

  return setmetatable(t, CameraSystem)
end

function CameraSystem:update(dt)
  local camera = self.entitiesCollection:firstWithTag('camera')
  local ship   = self.entitiesCollection:firstWithTag('player_ship')

  if not camera then return end
  if not ship   then return end

  if not camera.transformComponent then return end
  if not ship.transformComponent   then return end

  if camera.measuredVelocityComponent and ship.velocityComponent then
    camera.measuredVelocityComponent.vel = ship.velocityComponent.vel:dup()
  end

  camera.transformComponent.position = ship.transformComponent.position:dup()
end

return CameraSystem
