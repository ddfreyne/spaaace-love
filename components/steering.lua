local SteeringComponent = {}
SteeringComponent.__index = SteeringComponent

function SteeringComponent.new()
  local t = {
    isReversing    = false,
    isTurningLeft  = false,
    isTurningRight = false,
  }

  return setmetatable(t, SteeringComponent)
end

return SteeringComponent
