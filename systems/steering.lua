local SteeringSystem = {}
SteeringSystem.__index = SteeringSystem

function SteeringSystem.new(entitiesCollection)
  return setmetatable({ entitiesCollection = entitiesCollection }, SteeringSystem)
end

function SteeringSystem:update(delta)
  for entity in self.entitiesCollection:pairs() do
    self:updateEntity(entity, delta)
  end
end

function SteeringSystem:updateEntity(entity, delta)
  local transformComponent    = entity.transformComponent
  local accelerationComponent = entity.accelerationComponent
  local velocityComponent     = entity.velocityComponent
  local steeringComponent     = entity.steeringComponent

  if not steeringComponent then
    return
  end

  local angularVelocity = velocityComponent.maxAngularVelocity

  if velocityComponent then
    local vel = velocityComponent.vel
    if steeringComponent.isReversing then
      local angleDiff = Angle.normalize(vel:angle() - transformComponent.angle - math.pi)
      if math.abs(angleDiff) < 0.01 then
      elseif angleDiff < 0 then
        steeringComponent.isTurningLeft = true
      else
        steeringComponent.isTurningRight = true
      end
    end
  end

  if steeringComponent.isTurningLeft then
    transformComponent.angle = transformComponent.angle - angularVelocity * delta
  end

  if steeringComponent.isTurningRight then
    transformComponent.angle = transformComponent.angle + angularVelocity * delta
  end
end

return SteeringSystem
