local NameComponent = {}
NameComponent.__index = NameComponent

function NameComponent.new(name)
  local t = {
    name = name,
  }

  return setmetatable(t, NameComponent)
end

return NameComponent
