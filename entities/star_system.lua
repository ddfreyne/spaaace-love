local TransformComponent = require('components/transform')
local NameComponent      = require('components/name')

local StarSystem = {}
StarSystem.__index = StarSystem

function StarSystem.new(x, y, name)
  local entity = setmetatable({}, StarSystem)

  entity.transformComponent = TransformComponent.new(x, y, 0, 0)
  entity.nameComponent      = NameComponent.new(name)

  entity.planets = {}

  return entity
end

function StarSystem:addPlanet(planet)
  table.insert(self.planets, planet)
end

return StarSystem
