local Menu = {}
Menu.__index = Menu

local World     = require('entities/world')
local MainState = require('gamestates/main')
local Gamestate = require('vendor/hump/gamestate')
local Colors    = require('renderers/colors')
local Fonts     = require('renderers/fonts')

local stateName = 'menu'

require('engine/types')

function Menu.new()
  return setmetatable({}, Menu)
end

function Menu:init()
  local screenWidth   = love.window.getWidth()
  local buttonWidth   = 300
  local buttonHeight  = 80
  local buttonSpacing = 40

  local yOffset = 100
  local xOffset = screenWidth / 2 - buttonWidth / 2

  local newGameButton = loveframes.Create("button")
  newGameButton:SetState(stateName)
  newGameButton:SetPos(xOffset, yOffset)
  newGameButton:SetWidth(buttonWidth)
  newGameButton:SetHeight(buttonHeight)
  newGameButton:SetText("New Game")
  newGameButton.OnClick = function(object)
    world = World.new()
    mainState = MainState.new(world)
    Gamestate.switch(mainState)
  end

  local quitButton = loveframes.Create("button")
  quitButton:SetState(stateName)
  quitButton:SetPos(xOffset, yOffset + buttonSpacing + buttonHeight)
  quitButton:SetWidth(buttonWidth)
  quitButton:SetHeight(buttonHeight)
  quitButton:SetText("Quit")
  quitButton.OnClick = function(object)
    love.event.quit()
  end
end

function Menu:enter()
  loveframes.SetState(stateName)
end

function Menu:update(delta)
  loveframes.update(delta)
end

function Menu:draw()
  loveframes.draw()
end

function Menu:mousepressed(x, y, button)
  loveframes.mousepressed(x, y, button)
end

function Menu:mousereleased(x, y, button)
  loveframes.mousereleased(x, y, button)
end

function Menu:keypressed(key, unicode)
  loveframes.keypressed(key, unicode)
end

function Menu:keyreleased(key)
  loveframes.keyreleased(key)
end

return Menu
