local Entity = require('entities/entity')

local TransformComponent             = require('components/transform')
local AccelerationComponent          = require('components/acceleration')
local VelocityComponent              = require('components/velocity')
local SteeringComponent              = require('components/steering')
local HyperjumpComponent             = require('components/hyperjump')
local LocationComponent              = require('components/location')
local LogComponent                   = require('components/log')
local CommodityInventoryComponent    = require('components/commodity_inventory')
local StarSystemNavigationComponent  = require('components/star_system_navigation')
local MinimapRepresentationComponent = require('components/minimap_representation')

local ImageRenderingComponent        = require('components/rendering/image')
local AccelerationRenderingComponent = require('components/rendering/acceleration')

require('engine/types')

local Ship = {}
Ship.__index = Ship

function Ship.new(x, y, type, starSystem)
  local entity = Entity.new()

  entity:setTag('player_ship')

  entity.transformComponent             = TransformComponent.new(x, y, 1, 0)
  entity.velocityComponent              = VelocityComponent.new(0, 0, type.maxVelocity, type.angularVelocity)
  entity.accelerationComponent          = AccelerationComponent.new(false, type.acceleration)
  entity.steeringComponent              = SteeringComponent.new()
  entity.locationComponent              = LocationComponent.new(starSystem)
  entity.logComponent                   = LogComponent.new()
  entity.commodityInventoryComponent    = CommodityInventoryComponent.new()
  entity.renderingComponent             = ImageRenderingComponent.new(type.image, type.imageRotation)
  entity.starSystemNavigationComponent  = StarSystemNavigationComponent.new()
  entity.minimapRepresentationComponent = MinimapRepresentationComponent.new(4)

  entity.children:add(
    {
      renderingComponent = AccelerationRenderingComponent.new(entity),
      transformComponent = TransformComponent.new(0, 0, 1, 0),
    }
  )

  return entity
end

return Ship
