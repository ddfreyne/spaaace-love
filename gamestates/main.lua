local HUDSpace   = require('spaces/hud')
local ArenaSpace = require('spaces/arena')
local Signal     = require('vendor/hump/signal')
local Gamestate  = require('vendor/hump/gamestate')

local Main = {}
Main.__index = Main

function Main.new(world)
  local currentStarSystem = world.ship.locationComponent.starSystem

  local arenaSpace = ArenaSpace.new(world, currentStarSystem)
  local hudSpace   = HUDSpace.new(world, arenaSpace)

  local t = {
    arenaSpace = arenaSpace,
    hudSpace   = hudSpace,
    events     = {},
  }

  Signal.register('hyperjump_exit', function(hyperjumpEntity, newStarSystem)
    -- TODO at some point in the future, compare hyperjumpEntity with player ship
    Gamestate.switch(Main.new(world))
  end)

  return setmetatable(t, Main)
end

function Main:update(dt)
  self.arenaSpace:update(dt)
  self.hudSpace:update(dt)
end

function Main:draw()
  self.arenaSpace:draw()
  self.hudSpace:draw()
end

function Main:keypressed(key)
  self.arenaSpace:keypressed(key)
  self.hudSpace:keypressed(key)
end

return Main
