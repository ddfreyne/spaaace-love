require('engine/types')

local MeasuredVelocityComponent = {}
MeasuredVelocityComponent.__index = MeasuredVelocityComponent

function MeasuredVelocityComponent.new()
  local t = {
    vel = Vector.new(0, 0),
  }

  return setmetatable(t, MeasuredVelocityComponent)
end

return MeasuredVelocityComponent
