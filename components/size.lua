require('engine/types')

local SizeComponent = {}
SizeComponent.__index = SizeComponent

function SizeComponent.new(size)
  local t = {
    size = size,
  }

  return setmetatable(t, SizeComponent)
end

function SizeComponent:containsPoint(point)
  local rect = Rect.new(
    -self.size.width / 2, -self.size.height / 2,
    self.size.width, self.size.height)

  return rect:containsPoint(point)
end

return SizeComponent
