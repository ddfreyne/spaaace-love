require('engine/types')

local TransformComponent = {}
TransformComponent.__index = TransformComponent

function TransformComponent.new(x, y, z, angle)
  local t = {
    position = Point.new(x, y),
    z        = z,
    angle    = angle,
  }

  -- TODO add scale

  return setmetatable(t, TransformComponent)
end

return TransformComponent
