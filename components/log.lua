local LogComponent = {}
LogComponent.__index = LogComponent

function LogComponent.new()
  local t = {
    lines = {},
  }

  return setmetatable(t, LogComponent)
end

function LogComponent:add(line)
  table.insert(self.lines, line)
end

return LogComponent
