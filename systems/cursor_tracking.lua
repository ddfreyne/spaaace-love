local CursorTrackingSystem = {}
CursorTrackingSystem.__index = CursorTrackingSystem

function CursorTrackingSystem.new(entitiesCollection)
  local t = {
    entitiesCollection = entitiesCollection,
  }

  return setmetatable(t, CursorTrackingSystem)
end

function CursorTrackingSystem:update(dt)
  for entity in self.entitiesCollection:pairs() do
    self:updateEntity(entity, delta)
  end
end

function CursorTrackingSystem:updateEntity(entity, dt)
  local cursorTrackingComponent = entity.cursorTrackingComponent
  local transformComponent      = entity.transformComponent
  local sizeComponent           = entity.sizeComponent

  if not cursorTrackingComponent then return end
  if not transformComponent      then return end
  if not sizeComponent           then return end

  local x, y = love.mouse.getPosition()
  local translatedCursorPos = Point.new(
    x - transformComponent.position.x,
    y - transformComponent.position.y)

  cursorTrackingComponent.isCursorInside =
    sizeComponent:containsPoint(translatedCursorPos)
end

return CursorTrackingSystem
