local CommodityInventoryComponent = {}
CommodityInventoryComponent.__index = CommodityInventoryComponent

local function random(min, max)
  return min + math.floor(math.random() * (max - min))
end

function CommodityInventoryComponent.new()
  local t = {
    amounts = {}
  }

  return setmetatable(t, CommodityInventoryComponent)
end

return CommodityInventoryComponent
