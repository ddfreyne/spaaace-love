# Spaaace (LÖVE version)

This is a game in the same vein as Escape Velocity.

## Attribution

* [Planet sprites](http://opengameart.org/content/20-planet-sprites) by Justin Nichol, CC-BY 3.0.
* [Spaceship](http://millionthvector.blogspot.de/p/free-sprites.html) by MillionthVector, CC-BY 4.0 International.
* [Spaceships](http://kenney.nl/assets) by Kenney, CC-0

Planet detail images:

* _Desert Pavement_ by [aneyefortexas](https://www.flickr.com/people/aneyefortexas/)
* _City Glow_ by [hawkinsdigital](https://www.flickr.com/people/hawkinsdigital/)
* _A Clear Day at the Ocean- Busan, S. Korea_ by [bibbit](https://www.flickr.com/people/bibbit/)
* _lava meets ocean_ by [secret_canadian](https://www.flickr.com/people/secret_canadian/)
* _Little house on the tundra 8-11-05_ by [localsurfer](https://www.flickr.com/people/localsurfer/)
