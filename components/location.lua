require('engine/types')

local LocationComponent = {}
LocationComponent.__index = LocationComponent

function LocationComponent.new(starSystem)
  local t = {
    starSystem = starSystem,
    planet     = nil,
  }

  return setmetatable(t, LocationComponent)
end

return LocationComponent
