local Signal = require('vendor/hump/signal')

local Starfield = {}
Starfield.__index = Starfield

--[[

This thing requires:

* ship position (for seeding)
* ship velocity (for line length)
* ship hyperspace component (for pre-glow)
* ship location component (for seeding… fairly optional though)

Ship position can become camera position. Ship velocity could become camera
velocity if the camera had a velocity, but that would make it subject to the
physics system. Maybe we need a MeasuredVelocity component?

The pre- post-glow from hyperjumps should not be drawn here. If not here, I am
not sure where.

]]

-- TODO move random code somewhere else

local seed = 0

local function random()
  local r = math.sin(seed) * 37001;
  seed = seed + 1
  return r - math.floor(r)
end

local function randomStarColor()
  local r = random()
  local factor = (1.0 - r / 5)

  if r < 0.5 then -- blue
    return 255 * factor, 255 * factor, 255, 255
  else -- yellow
    return 255, 255, 255 * factor, 255
  end
end

local function randomDistantStarColor()
  local r, g, b, a = randomStarColor()
  return r, g, b, a / 2
end

local function randomDustColor()
  return 192, 128, 0, 255
end

--

function Starfield.new(ship, entitiesCollection)
  local t = {
    rootSeed = math.floor(math.random() * 1000),
    ship = ship,
    entitiesCollection = entitiesCollection,
  }

  Signal.register('hyperjump_exit', function(hyperjumpEntity, starSystem)
    -- TODO at some point in the future, compare hyperjumpEntity with player ship
    t.hyperjumpExitTime = love.timer.getTime()
  end)

  return setmetatable(t, Starfield)
end

function Starfield:draw(betweenFn)
  self:drawPreHyperjumpGlow()
  self:drawPostHyperjumpGlow()

  -- distant stars
  self:drawPlane(1000, 0.000100,  1, randomDistantStarColor)
  self:drawPlane(800,  0.000100,  1, randomDistantStarColor)
  self:drawPlane(600,  0.000010,  1, randomStarColor)
  self:drawPlane(400,  0.000010,  1, randomStarColor)
  self:drawPlane(200,  0.000010,  1, randomStarColor)

  -- near stars
  self:drawPlane(100,  0.000005, 3, randomStarColor)
  self:drawPlane(90,   0.000005, 2, randomStarColor)
  self:drawPlane(80,   0.000005, 2, randomStarColor)
  self:drawPlane(70,   0.000005, 2, randomStarColor)
  self:drawPlane(60,   0.000005, 2, randomStarColor)
  self:drawPlane(50,   0.000005, 1, randomStarColor)

  -- dust
  self:drawPlane(1.2,  0.000003, 1, randomDustColor)
  self:drawPlane(1.1,  0.000003, 1, randomDustColor)
  betweenFn()
  self:drawPlane(1,    0.000003, 1, randomDustColor)
  self:drawPlane(0.9,  0.000003, 1, randomDustColor)
end

function Starfield:drawPreHyperjumpGlow()
  local hyperjumpComponent = self.ship.hyperjumpComponent
  if not hyperjumpComponent then return end

  local alpha = math.floor(math.pow(hyperjumpComponent.progress, 8) * 255 / 3)

  love.graphics.setColor(255, 255, 255, alpha)
  love.graphics.rectangle('fill', 0, 0, love.window.getWidth(), love.window.getHeight())
end

function Starfield:drawPostHyperjumpGlow()
  local now  = love.timer.getTime()
  local prev = self.hyperjumpExitTime

  if not prev then
    return
  end

  local diff = now - prev

  if diff < 1.0 then
    love.graphics.setColor(255, 255, 255, math.floor((1 - diff) * 255))
    love.graphics.rectangle('fill', 0, 0, love.window.getWidth(), love.window.getHeight())
  end
end

local function centerOnPointWhile(point, fn)
  love.graphics.push()

  local dx = love.window.getWidth()  / 2 - point.x
  local dy = love.window.getHeight() / 2 - point.y
  love.graphics.translate(dx, dy)
  fn()

  love.graphics.pop()
end

function Starfield:drawPlane(parallax, density, weight, colorFn)
  local shipPos = self.ship.transformComponent.position
  local point = shipPos:asVector():div(parallax):asPoint()

  centerOnPointWhile(point, function()
    local windowWidth  = love.window.getWidth()
    local windowHeight = love.window.getHeight()

    local originX = point.x - windowWidth  / 2
    local originY = point.y - windowHeight / 2

    local left = math.floor(originX / windowWidth)  * windowWidth
    local top  = math.floor(originY / windowHeight) * windowHeight

    love.graphics.push()
    love.graphics.translate(left, top)
    self:drawLocal(0, 0, point, parallax, density, weight, colorFn)
    self:drawLocal(0, 1, point, parallax, density, weight, colorFn)
    self:drawLocal(1, 0, point, parallax, density, weight, colorFn)
    self:drawLocal(1, 1, point, parallax, density, weight, colorFn)
    love.graphics.pop()
  end)
end

function Starfield:_localSeed(screenDX, screenDY, point, parallax)
  local windowWidth  = love.window.getWidth()
  local windowHeight = love.window.getHeight()

  local originX = point.x - windowWidth  / 2
  local originY = point.y - windowHeight / 2

  local scaledOriginX = math.floor(originX / windowWidth);
  local scaledOriginY = math.floor(originY / windowHeight);

  return self.rootSeed +
         (scaledOriginX + screenDX) * 100 +
         (scaledOriginY + screenDY) * 10000 +
         parallax                   * 1000000
end

function Starfield:drawLocal(screenDX, screenDY, point, parallax, density, weight, colorFn)
  local windowWidth  = love.window.getWidth()
  local windowHeight = love.window.getHeight()

  seed = self:_localSeed(screenDX, screenDY, point, parallax)

  love.graphics.push()
  love.graphics.translate(screenDX * windowWidth, screenDY * windowHeight)
  local amountOfStars = density * windowWidth * windowHeight
  for i = 1, amountOfStars do
    local starX      = random() * windowWidth
    local starY      = random() * windowHeight
    local starRadius = math.ceil(random() * weight)

    -- TODO check for presence of entity and component
    local velocity = self.entitiesCollection:firstWithTag('camera').measuredVelocityComponent.vel
    local velocityFactor = velocity:getLength() / parallax * 1
    velocityFactor = math.min(velocityFactor, 100)
    if math.abs(velocityFactor) < 1.0 then
      velocityFactor = 1.0
    end

    r, g, b, a = colorFn()
    love.graphics.setColor(r, g, b, (a / 255) * (255 - velocityFactor * 1.5))

    local endX = starX - velocity:cos() * velocityFactor
    local endY = starY - velocity:sin() * velocityFactor
    love.graphics.setLineWidth(starRadius)
    love.graphics.line(starX, starY, endX, endY)
  end
  love.graphics.pop()
end

return Starfield
