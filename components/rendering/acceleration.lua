local AccelerationRenderingComponent = {}
AccelerationRenderingComponent.__index = AccelerationRenderingComponent

function AccelerationRenderingComponent.new(subjectEntity)
  local t = {
    progress = 0,
    subjectEntity = subjectEntity,
  }

  return setmetatable(t, AccelerationRenderingComponent)
end

function AccelerationRenderingComponent:render(entity)
  local accelerationComponent = self.subjectEntity.accelerationComponent
  if not accelerationComponent then return end

  local hyperjumpComponent = self.subjectEntity.hyperjumpComponent

  if not accelerationComponent.isAccelerating and (not hyperjumpComponent or hyperjumpComponent.progress < 0.00001) then return end

  local w = 99
  local h = 75

  love.graphics.push()
  love.graphics.scale(w/3, h/3)

  love.graphics.setColor(255, 192, 0, 255)
  love.graphics.polygon(
    "fill",
    -0.5, 0.5,
    -0.5, -0.5,
    -1.5 + math.sin(self.progress * 50) * 0.1, 0)

  love.graphics.setColor(255, 64, 0, 255)
  love.graphics.polygon(
    "fill",
    -0.5, 0.5,
    -0.5, -0.5,
    -1.3 + math.sin(self.progress * 50) * 0.1, 0)

  love.graphics.pop()
end

return AccelerationRenderingComponent
