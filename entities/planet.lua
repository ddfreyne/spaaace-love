local TransformComponent             = require('components/transform')
local ImageRenderingComponent        = require('components/rendering/image')
local PlanetKindComponent            = require('components/planet_kind')
local NameComponent                  = require('components/name')
local CommoditiesComponent           = require('components/commodities')
local MinimapRepresentationComponent = require('components/minimap_representation')

local Planet = {}
Planet.__index = Planet

function Planet.new(x, y, image)
  local entity = {}

  -- TODO make configurable
  local detailImage = love.graphics.newImage("assets/planet-views/desert.jpg")
  local name = "53 Virgo"

  entity.transformComponent             = TransformComponent.new(x, y, 0, 0)
  entity.renderingComponent             = ImageRenderingComponent.new(image, 0)
  entity.planetKindComponent            = PlanetKindComponent.new(detailImage)
  entity.nameComponent                  = NameComponent.new(name)
  entity.commoditiesComponent           = CommoditiesComponent.new()
  entity.minimapRepresentationComponent = MinimapRepresentationComponent.new(4)

  return entity
end

return Planet
