local AccelerationComponent = {}
AccelerationComponent.__index = AccelerationComponent

function AccelerationComponent.new(isAccelerating, maxAcceleration)
  local t = {
    isAccelerating  = isAccelerating,
    progress        = 0,
    maxAcceleration = maxAcceleration,
  }

  return setmetatable(t, AccelerationComponent)
end

return AccelerationComponent
