local CreditsComponent = {}
CreditsComponent.__index = CreditsComponent

function CreditsComponent.new(amount)
  local t = {
    amount = amount,
  }

  return setmetatable(t, CreditsComponent)
end

return CreditsComponent
