local PlanetKindComponent = {}
PlanetKindComponent.__index = PlanetKindComponent

function PlanetKindComponent.new(detailImage)
  local t = {
    detailImage = detailImage,
  }

  return setmetatable(t, PlanetKindComponent)
end

return PlanetKindComponent
