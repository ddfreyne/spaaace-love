local MinimapRepresentationComponent = {}
MinimapRepresentationComponent.__index = MinimapRepresentationComponent

function MinimapRepresentationComponent.new(diameter)
  local t = {
    diameter = diameter,
  }

  return setmetatable(t, MinimapRepresentationComponent)
end

return MinimapRepresentationComponent
