local Signal = require('vendor/hump/signal')
require 'engine/types'

local HyperjumpSystem = {}
HyperjumpSystem.__index = HyperjumpSystem

function HyperjumpSystem.new(entitiesCollection)
  return setmetatable({ entitiesCollection = entitiesCollection }, HyperjumpSystem)
end

function HyperjumpSystem:update(delta)
  for entity in self.entitiesCollection:pairs() do
    self:updateEntity(entity, delta)
  end
end

function HyperjumpSystem:updateEntity(entity, delta)
  local hyperjumpDuration = 5 -- in seconds

  local hyperjumpComponent            = entity.hyperjumpComponent
  local starSystemNavigationComponent = entity.starSystemNavigationComponent
  local transformComponent            = entity.transformComponent
  local velocityComponent             = entity.velocityComponent
  local steeringComponent             = entity.steeringComponent
  local accelerationComponent         = entity.accelerationComponent
  local locationComponent             = entity.locationComponent

  if not steeringComponent             then return end
  if not transformComponent            then return end
  if not velocityComponent             then return end
  if not accelerationComponent         then return end
  if not hyperjumpComponent            then return end
  if not locationComponent             then return end
  if not starSystemNavigationComponent then return end

  -- take control
  steeringComponent.isTurningRight = false
  steeringComponent.isTurningLeft = false
  accelerationComponent.isAccelerating = false

  -- calculate angle
  local posA = locationComponent.starSystem.transformComponent.position
  local posB = starSystemNavigationComponent.destination.transformComponent.position
  local desiredAngle = posA:vectorTo(posB):angle()

  -- align
  local angleDiff = Angle.normalize(transformComponent.angle - desiredAngle)
  if angleDiff > 0.01 then
    steeringComponent.isTurningLeft = true
    return
  elseif angleDiff < -0.01 then
    steeringComponent.isTurningRight = true
    return
  end

  -- accelerate
  local vel = velocityComponent.vel
  local prog = hyperjumpComponent.progress
  vel.x = vel.x + math.cos(desiredAngle) * math.pow(prog * hyperjumpDuration, 8)
  vel.y = vel.y + math.sin(desiredAngle) * math.pow(prog * hyperjumpDuration, 8)

  -- prevent float overflow
  local pos = transformComponent.position
  local distanceLimit  = 10000000
  local distanceOffset = 100000
  if pos.x >  distanceLimit then pos.x =  distanceOffset + pos.x % distanceLimit end
  if pos.y >  distanceLimit then pos.y =  distanceOffset + pos.y % distanceLimit end
  if pos.x < -distanceLimit then pos.x = -distanceOffset - pos.x % distanceLimit end
  if pos.y < -distanceLimit then pos.y = -distanceOffset - pos.y % distanceLimit end

  -- update
  hyperjumpComponent.progress = hyperjumpComponent.progress + delta / hyperjumpDuration

  -- done
  if hyperjumpComponent.progress > 1.0 then
    -- arrive
    locationComponent.starSystem = starSystemNavigationComponent.destination

    -- exit hyperjump
    entity.hyperjumpComponent = nil
    starSystemNavigationComponent.destination = nil

    -- Notify
    Signal.emit('hyperjump_exit', entity, locationComponent.starSystem)

    -- set velocity
    velocityComponent.vel:limitToLength(velocityComponent.maxVelocity)

    -- set position outside of system center
    local vel = velocityComponent.vel
    local normalizedVel = vel:div(vel:getLength())
    transformComponent.position.x = - normalizedVel.x * 7500
    transformComponent.position.y = - normalizedVel.y * 7500
  end
end

return HyperjumpSystem
