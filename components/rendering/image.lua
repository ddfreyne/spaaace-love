local ImageComponent = {}
ImageComponent.__index = ImageComponent

function ImageComponent.new(image, rotation)
  local t = {
    image = image,
    rotation = rotation,
  }

  return setmetatable(t, ImageComponent)
end

function ImageComponent:render(entity)
  local w = self.image:getWidth()
  local h = self.image:getHeight()

  love.graphics.rotate(self.rotation)
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.draw(self.image, -w/2, -h/2)
end

return ImageComponent
