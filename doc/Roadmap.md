# Roadmap

## Main screen

Buttons that we’ll eventually have: New game, load game, options, credits, quit.

1. **[DONE]** New game button
1. **[DONE]** Quit button
1. New game button that asks for the player name

Other stuff is out of scope for now.

## Planet landing screen

1. **[DONE]** Add pretty picture
1. **[DONE]** Add planet name
1. **[DONE]** Add short planet description (placeholder text)

## Trading

There will be a very basic fake economy that allows trading. A more advanced economy is out of scope.

1. **[DONE]** Player starts with a sum of money, shown in the HUD
1. **[DONE]** Trade button is added to planet detail view
1. **[DONE]** Planets have commodity prices assigned, shown in the land screen
1. **[DONE]** Player can buy and sell infinite amounts
1. **[DONE]** Player can buy and sell limited by credits
1. Ship has limited storage space
