local Fonts = {}
Fonts.__index = Fonts

Fonts.head   = love.graphics.newFont('assets/fonts/ClearSans-Bold.ttf', 36)
Fonts.title  = love.graphics.newFont('assets/fonts/ClearSans-Bold.ttf',    24)
Fonts.body   = love.graphics.newFont('assets/fonts/ClearSans-Regular.ttf', 18)
Fonts.label  = love.graphics.newFont('assets/fonts/ClearSans-Regular.ttf', 16)
Fonts.button = Fonts.title

Fonts.hudBoxContentSmall = love.graphics.newFont('assets/fonts/ClearSans-Bold.ttf', 18)
Fonts.hudBoxContentBig   = love.graphics.newFont('assets/fonts/ClearSans-Bold.ttf', 24)
Fonts.hudBoxLabel        = love.graphics.newFont('assets/fonts/ClearSans-Bold.ttf', 18)

return Fonts
