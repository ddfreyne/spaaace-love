local TransformComponent = require('components/transform')

require('engine/types')

local Entity = {}
Entity.__index = Entity

function Entity.new()
  local entity = setmetatable({}, Entity)

  entity.children = Set.new()

  return entity
end

function Entity:setTag(tag)
  self.tag = tag
end

function Entity:getTag()
  return self.tag
end

return Entity
