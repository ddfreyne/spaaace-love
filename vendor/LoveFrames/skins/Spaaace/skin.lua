local skin = {}

skin.name    = 'Spaaace'
skin.author  = 'Denis Defreyne'
skin.version = '0.1'
skin.base    = 'Orange'

local Colors = require('renderers/colors')
local Fonts  = require('renderers/fonts')

skin.controls = {}

skin.controls.border_color     = Colors.semiDarkCyan
skin.controls.button_text_font = Fonts.title
skin.controls.panel_body_color = Colors.darkCyan

local bordercolor = { 143, 143, 143, 255 }

function skin.DrawButton(object)
  local skin                  = object:GetSkin()
  local x                     = object:GetX()
  local y                     = object:GetY()
  local width                 = object:GetWidth()
  local height                = object:GetHeight()
  local hover                 = object:GetHover()
  local text                  = object:GetText()
  local font                  = object.font or skin.controls.button_text_font
  local twidth                = font:getWidth(object.text)
  local theight               = font:getHeight(object.text)
  local down                  = object.down
  local enabled               = object:GetEnabled()
  local clickable             = object:GetClickable()

  -- button body
  if not enabled or not clickable then
    Colors.set(Colors.darkCyan)
  elseif down then
    Colors.set(Colors.cyan)
  elseif hover then
    Colors.set(Colors.semiDarkCyan)
  else
    Colors.set(Colors.darkCyan)
  end
  love.graphics.rectangle('fill', x, y, width, height)

  -- button text
  love.graphics.setFont(font)
  if not enabled or not clickable then
    Colors.set(Colors.semiDarkCyan)
  elseif down then
    Colors.set(Colors.black)
  elseif hover then
    Colors.set(Colors.white)
  else
    Colors.set(Colors.white)
  end
  love.graphics.print(text, x + width/2 - twidth/2, y + height/2 - theight/2)
end

function skin.DrawPanel(object)
  local skin        = object:GetSkin()
  local x           = object:GetX()
  local y           = object:GetY()
  local width       = object:GetWidth()
  local height      = object:GetHeight()
  local bodyColor   = skin.controls.panel_body_color
  local borderColor = skin.controls.border_color

  love.graphics.setColor(bodyColor)
  love.graphics.rectangle("fill", x, y, width, height)

  love.graphics.setColor(borderColor)
  skin.OutlinedRectangle(x, y, width, height)
end

loveframes.skins.Register(skin)
