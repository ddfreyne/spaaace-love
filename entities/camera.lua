local Entity = require('entities/entity')
local TransformComponent = require('components/transform')
local MeasuredVelocityComponent = require('components/measured_velocity')

local Camera = {}
Camera.__index = Camera

function Camera.new(x, y)
  local entity = Entity.new()

  entity.transformComponent        = TransformComponent.new(x, y, 0, 0)
  entity.measuredVelocityComponent = MeasuredVelocityComponent.new()

  entity:setTag('camera')

  return entity
end

return Camera
