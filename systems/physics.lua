local PhysicsSystem = {}
PhysicsSystem.__index = PhysicsSystem

function PhysicsSystem.new(entitiesCollection)
  return setmetatable({ entitiesCollection = entitiesCollection }, PhysicsSystem)
end

function PhysicsSystem:update(delta)
  for entity in self.entitiesCollection:pairs() do
    self:updateEntity(entity, delta)
  end
end

function PhysicsSystem:updateEntity(entity, delta)
  local transformComponent    = entity.transformComponent
  local accelerationComponent = entity.accelerationComponent
  local velocityComponent     = entity.velocityComponent

  if not transformComponent then return end
  if not velocityComponent  then return end

  local isAccelerating = accelerationComponent and accelerationComponent.isAccelerating
  if isAccelerating then
    c = math.cos(transformComponent.angle)
    s = math.sin(transformComponent.angle)

    velocityComponent.vel.x = velocityComponent.vel.x + c * delta * accelerationComponent.maxAcceleration
    velocityComponent.vel.y = velocityComponent.vel.y + s * delta * accelerationComponent.maxAcceleration

    velocityComponent.vel:limitToLength(velocityComponent.maxVelocity)
  end

  transformComponent.position.x = transformComponent.position.x + velocityComponent.vel.x
  transformComponent.position.y = transformComponent.position.y + velocityComponent.vel.y
end

return PhysicsSystem
