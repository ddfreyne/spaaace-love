require('engine/types')

local ShipType   = require('entities/ship_type')
local Ship       = require('entities/ship')
local Planet     = require('entities/planet')
local Player     = require('entities/player')
local Camera     = require('entities/camera')
local StarSystem = require('entities/star_system')

local World = {}
World.__index = World

local function random(min, max)
  return min + math.floor(math.random() * (max - min))
end

local names = { "Antiphanes", "Patil", "Gismonda", "Maurilia", "Undset", "Yennydieguez", "Hennigar", "Clouet", "Edgerton", "Susanoo", "Quantz", "Preziosa", "Toland", "Wolfernst", "Zachariassen", "Fisher", "Liselotte", "Yukifumi", "Pepping", "Prigogine", "Aramis", "Sakuranosyou", "Halperin", "Heckmann", "Haraldlesch", "Denversmith", "Yidaeam", "Helwerthia", "Abramenko", "Deliyannis", "Kendriddle", "Linsley", "Shukshin", "Sariancel", "Witten", "Ahn", "Johnratje", "Corvina", "Kazak", "Ambiorix", "Gladys", "Fernandonido", "Bathilde", "Jefsoulier", "Alexacourtis", "Oceana", "Fumiofuke", "Aoki", "Nabrown", "Okushiri", "Fandly", "Gubbio", "Malna", "Joaoalves", "Zoyamironova", "Ponomarev", "Hertha", "Birlan", "Pushkin", "Shorland", "Kevola", "Sisigambis", "Obsfabra", "Raine", "Combes", "Bandos", "Lindsayleona", "Ericthomas", "Ingwelde", "Kimwallin", "Heliaca", "Haviland", "Danby", "Vogelweide", "Nicolashayek", "Ashida", "Lindner", "Santhikodali", "Helmholtz", "Stefanovalentini", "Susannesandra", "Marychristie", "Tsujitsuka", "Mattei", "Skytel", "Ratripathi", "Dancingangel", "Mayakovsky", "Jaffe", "Tipografiyanauka", "Stattmayer", "Londontario", "Mezentsev", "Deikoon", "Okugi", "Klyuchevskaya", "Joyhines", "Louwman", "van Eyck", "Liaoning", "Nicjachowski", "Odysseus", "DeLaeter", "Kubacek", "Spencer", "Ellendoane", "Jongwon", "Bancilhon", "Mindaugas", "Vsekhsvyatskij", "Mikemcdowell", "Eugster", "Vasks", "Cloyd", "Roberteunice", "Biela", "Slouka", "Hochlehnert", "Triceratops", "Gerlinde", "Fan", "Brucestephenson", "Virginia", "Buthiers", "Murgas", "Turing", "Skalnate Pleso", "Handley", "Inca", "Lundia", "Dodelson", "Merian", "Josephhenry", "Chivilikhin", "Coltrane", "Hocking", "Stevemorgan", "Emileschweitzer", "Tikhonov", "Freudenthal", "Sparker", "Estelleweber", "Jenniferling", "van Zee", "Misik", "Raymond", "Chaplin", "Joshuajones", "Boyan", "Bertram", "Bora-Bora", "Fisico", "Lindaweiland", "Taesch", "Carriehudson", "Mandelshtam", "Michaeloy", "Perepadin", "Yumikoitahana", "Sebastiana", "Medea", "Llano", "Teneriffa", "Avidzba", "Silvain", "Probitas", "van Sprang", "Paquifrutos", "Belgrano", "Piani", "Disneya", "Kuniko", "Scythia", "Carolsmyth", "Ichinohe", "Ritageorge", "Pickard", "Sikorsky", "Sawilliams", "Shaposhnikov", "Lawson", "Beyer", "Quaide", "Kuiper", "Mitlincoln", "Cavaillon", "Santa", "Lewotsky", "Meesters", "Alphonsina", "Kurtbachmann", "Yaoan", "Orchiston", "Attica", "Perdix", "Irpedina", "Majoni", "Jimstratton", "Fuzhougezhi", "Leviathan", "Rubtsov", "Sicardy", "Johnhault", "Fleurmaxwell", "Shibata", "Rakuyou", "Dodoens", "Rottmann", "Kamisaibara", "Yumiko", "Belleau", "Sesar", "Leopoldjosefine", "Selene", "Bobshelton", "Laurierumker", "McGee", "Gross", "Nireus", "Brage", "Leibniz", "Graetz", "Reinerstoss", "Idelsonia", "Saitama", "Christopynn", "Buzzi", "Krasnoyarsk", "Frejakocha", "Ullmann", "Pucinskas", "Israel", "Hutchings", "Diehl", "Sosva", "Dennisritchie", "Kellyriedell", "Gerarda", "Carolinakou", "Neelpatel", "Duke", "Michaelyurko", "Griffis", "Polakis", "Naef", "Kandola", "Devanssay", "Evanfletcher", "Kothen", "Yoshinori", "Monnig", "Pietchisson", "Recawkwell", "Robbynaish", "Safarik", "Andrewmerrill", "Ludolfschultz", "Bryanhe", "Kalygeringer", "Roquebrune", "Hollyerickson", "Marci", "Franspost", "Liedeke", "Joannemichet", "Naveenmurali", "Vinifera", "Dione", "Berkeley", "Cuvier", "Korkosz", "Baumhauer", "Robersomma", "Shrock", "Domatthews", "Srbija", "Letunov", "Ashkova", "McAllister", "Suvanto", "Bustersikes", "Keyaki", "Benbrewer", "Thales", "Qoyllurwasi", "Leifer", "Abante", "Gertfinger", "Karpov", "Asirvatham", "Kibirev", "Burdett", "Kanba", "Aenona", "Eurydamas", "Alineraynal", "Zuckmayer", "Sigune", "Danielarhodes", "Willemkolff", "Jarnefelt", "Jennamarie", "Seneferu", "Haswell", "Trusheim", "Stanshapiro", "Vasadze", "Sif", "Janemojo", "Palus", "Valemangano", "Xinwang", "Choseikomori", "Evanichols", "Katotsuyoshi", "McCarron", "Van Dijck", "Juliekrugler", "Shantanugaur", "Votroubek", "Juliusbreza", "MacLean", "Loremaes", "Sofiyavarzar", "Worpswede", "Eboshiyamakouen", "Penghuanwu", "Kimuranaoto", "Oravetz", "Velikhov", "Whither", "Agrawain", "Neleus", "De Martin", "Francini", "Rawson", "Owen", "Mecklenburg", "Terradas", "Milos", "van Woerkom", "Cilla", "Ralpharvey", "Hemmerechts", "Basso", "Gaillard", "Boubin", "Susanlederer", "Sterken", "Cleynaerts", "Davidlynch", "Steffl", "Marina", "Koyama", "Goldman", "Ywain", "Choikaiyau", "Lasorda", "Tomatic", "Westerlund", "Rileyennis", "Demeter", "Hannover", "Changchun", "Esterantonucci", "Phildeutsch", "Yuriosipov", "Komiyama", "Fengduan", "Snyder", "Tokara", "Genichiaraki", "Chephren", "Kreisau", "Gryphia", "Dianeingrao", "Silesia", "Kaluga", "Frasercain", "Boulanger", "Schuyler", "Chengmaolan", "Veteraniya", "Turtlestar", "Kobedaitenken", "Sudo", "Donnadean", "Thangada", "Wongshingsheuk", "Demokritos", "Shimanamikaido", "Myslbek", "Mesopotamia", "Mitidika", "Baggesen", "Francopacini", "Kutaissi", "Ampzing", "Chrisbondi", "Sprowls", "Hormuthia", "Ajstewart", "Sfranke", "Yuyakekoyake", "Billallen", "Zhuravleva", "Yokokurayama", "Ketover", "Cima Rest", "Yes", "Briancarey", "Zurria", "Namahage", "Stavropolis", "Gregneumann", "Bagration", "Rephiltim", "Elkins-Tanton", "Gellivara", "Siyilee", "Pugnax", "Kopff", "Schuster", "Bateman", "Robertmoore", "Hildakowalski", "Peterkraft", "Automedon", "Hirai", "Isobelthompson", "Caupolican", "Itagaki", "Kimdongyoung", "Olegbykov", "Podjavorinska", "Lafcadio", "Grier", "Thoas", "Frithjof", "Richlee", "Simferopol", "Berko", "Piccolo", "Dimare", "Michdelucia", "Christikeen", "Ur", "Boulder", "Helma", "Brlka", "Sungwoncho", "Bohnenblust", "Kevinellis", "Luga", "Aksnes", "Berlind", "Abshir", "Elizawoolard", "Denlea", "Prime", "Schiaparelli", "ASP", "Sax", "Shinsubin", "Jorvik", "Retopezzoli", "Arcadia", "Alanschorn", "Blanton", "Lucabracali", "Kuniharu", "Belinskij", "Laurensmith", "Tvaruzkova", "Stenkyrka", "Aretta", "Duannihuang", "Antarctica", "Lavoratti", "Solf", "Rem", "Delisle", "Ajjarapu", "Dancey", "Levasseur", "Ussery", "Ranke", "Almary", "Dryope", "McBride", "Margaretmiller", "Seansolomon", "Morstadt", "Elduval", "Pradun", "Pochaina", "Karlin", "Patcassen", "Raab", "Tarsila", "Roadrunner", "Campanula", "Waynerichie", "Forsius", "Bakosgaspar", "Brozovic", "Purpurea", "Hancock", "Olieslagers", "Kirsan", "Cohen", "Bagryana", "Petereisenhardt", "Tarcisiozani", "Vonderheydt", "Joanrho", "Hellahaasse", "Flamini", "Comello", "Cameron", "Bibracte", "Messerschmidt", "Koschny", "Jiranek", "Kawamura", "Wayneclark", "Annelemaitre", "Deborahdomingue", "Micromegas", "Kumon", "Rieugnie", "Hoher List", "Shirao", "Holmhallar", "Kaarina", "Jonbach", "Barnowl", "Paijanne", "Alexkeeler", "Suminao", "Hynek", "Ohm", "Holmes", "Menchu", "Gokcay", "Okuda", "Siran", "Kakkuri", "Bouillabaisse", "Irkutsk", "Basilea", "Cliffordkim", "Mimiyen", "Heuberger", "Forrestbetton", "Kenya", "Sutton", "Seilandfarm", "Orenbrecher", "Stichius", "Halawe", "Alvin", "Lilith", "Caseylisse", "Kliment Ohridski", "Moldavia", "Puskas", "Rimito", "Ivanpaskov", "Battisti", "Armor", "Brenda", "Rudolf", "Krosigk", "Alicelandis", "Plate", "Lugmair", "Muzzio", "Sylrobertson", "Delia", "Sigenori", "Moore", "Liebermann", "Chagas", "Angelalouise", "Beadell", "Rhipeus", "Hiroshimanabe", "Nove Hrady", "Vera", "Berger", "Yugra", "Chodas", "Oken", "Fertoszentmiklos", "Lanzerotti", "Konstitutsiya", "Portlandia", "Lintingnien", "Italocalvino", "Wallenstadt", "Torquilla", "Dustindeford", "Saint-Marys", "Laval", "Caleyo", "Wichmann", "Williwaw", "Goldstone", "San Jose", "Demalia", "Brest", "Guiraudon", "Rhodope", "Piestany", "Kenmitchell", "Oaxaca", "Vaintrob", "Cindijon", "Nichol", "Reneracine", "Bocarsly", "Milton", "Ogawakiyoshi", "De Meis", "Buffon", "Akaishidake", "Sukunabikona", "Sheldon", "Huth", "Yingling", "Bernacca", "Alyssehrlich", "Pottasch", "Ivanopaci", "Doudantsutsuji", "Weber", "Kristina", "Jaynethomp", "Jessonda", "Toyokawa", "von Wrangel", "Schramm", "Caropietsch", "Bettyphilpotts", "Svoboda", "Chapman", "Michaelsegal", "Menelaus", "Kamel", "Pepita", "Duboshin", "Yuanfengfang", "Plantin", "Zolotov", "Trinkle", "Bernini", "Gal", "Angerhofer", "Yakiimo", "Hermitage", "Danyellelee", "Latona", "Uchikawa", "Ottmarsheim", "Michaelwham", "Camillo", "Sirona", "Chelsealynn", "Yourcenar", "Anubhavguha", "Pesch", "Waltermaria", "Sheilawolfman", "Nicovincenti", "Petrovedomosti", "Trefftz", "Humperdinck", "Takayuki", "Orosei", "Jugurtha", "Theotes", "van de Kamp", "Mera", "Mercatali", "Metcalfia", "Valentaugustus", "Blow", "Lofgren", "Vesale", "Lydina", "Keneke", "Napolitania", "Beny", "Edwardsu", "Fennia", "Emilevasseur", "Samapaige", "Goncharov", "Babinkova", "Albinadubois", "Harlingten", "Zucker", "Herbertpalme", "Peruindiana", "Schaerding", "Kressida", "Arunvenkataraman", "Alamosa", "Patrickhsu", "Kunishimakoto", "Mimeev", "Sisiliang", "Stepciechan", "Anokhin", "Lebesgue", "Siegma", "Dalmatin", "Skirda", "Prilepina", "Ksenborisova", "Sedna", "Alanhale", "Koremori", "Benhughes", "Ciarabarr", "Ducentesima", "Palisana", "Chenjiansheng", "Arosa", "Hajos", "Hazel", "Hoshikawa", "Schnitger", "Borgman", "Koshiishi", "Lochner", "Moraes", "Phemios", "Nele", "Marybuda", "Wangzhaoxin", "Carlscheele", "Yangel", "Lisamichaels", "Lampetia", "Kaminokuni", "Kronstadt", "Kalinin", "Goldschmidt", "Fidrich", "La Condamine", "Winters", "Zengguoshou", "Visby", "Delaney", "Ivanova", "Ellicott", "Fulchignoni", "Fatme", "Stradivari", "Icarion", "Cerulli", "Alden", "Amun", "Imatra", "Wannes", "Oka", "Tornio", "Robberto", "Hohmann", "Samirsur", "Yukawa", "Kamimura", "Philiphe", "Petzval", "Hirundo", "Gromov", "Federicotosi", "Gurnikovskaya", "Bobgent", "La Orotava", "Chaos", "Charmawidor", "Gangkeda", "Allen", "Chemin", "Nyrola", "Patrickjones", "Punahou", "Faithchang", "Yurlov", "Modestia", "Montague", "Vsevoivanov", "Kanroku", "Popocatepetl", "Wallmuth", "Cisneros", "Okadanoboru", "Hippokoon", "McElroy", "Antink", "Waltimyer", "Idefix", "Genecampbell", "Kalahari", "Messenger", "Donasharma", "Glennford", "Dr. G.", "Chiminello", "Adamcurry", "Anantpatel", "Rykhlova", "Edithlevy", "Whitford", "Fama", "Changjiangcun", "Galapagos", "Marceline", "Goldstein", "Charton", "Polykletus", "Lemus", "Noto", "Cicek", "Paolotesi", "Fitzroy", "Pax", "Haolei", "Louise", "Marthahazen", "Semper", "Akiraabe", "Akesson", "Kodaly", "Glikson", "Owens", "Kathyclemmer", "Izumi", "Kimrichards", "Charconley", "Dahlgren", "Quirrenbach", "Amandahatton", "Pliska", "Helvetia", "Vogtia", "van Swinden", "Kholshevnikov", "Zwetana", "Ipatov", "Sussenbach", "Robertreeves", "Buxus", "Platonov", "Haupt", "van der Kruit", "Jenikhollan", "Felicitas", "Ivankrasko", "Moulding", "Fogh", "Klio", "Reginhild", "Margo", "Brecht", "Parijskij", "Benuri", "Siding Spring", "Fanatica", "Kaborchardt", "Mikequinn", "von Matt", "Amici", "Stiavnica", "Pintar", "Burbidge", "Jiri", "Iidetenmomdai", "Massachusetts", "Leewonchul", "Nassovia", "Klopstock", "Uyttenhove", "Vala", "Raissa", "Ljuba", "Nishimoto", "Hainan", "Maniewski", "Bechtel", "Brasilia", "Nametoko", "Jeanpaul", "Billary", "Booth", "Robertwick", "Vilas", "Thomana", "Camoes", "Wubbena", "Anneres", "Evgenilebedev", "Pivatte", "Janboda", "Pollath", "Dow", "Tokeidai", "Pfau", "Hallerstein", "Echepolos", "Richnelson", "Leverrier", "Hawke", "Novosibirsk", "Sciurus", "Plimpton", "Steins", "Fionawood", "Augeias", "Isabelhawkins", "Lena", "Keenanmonks", "Manning", "Smoluchowski", "Adelaide", "Cibronen", "Eryan", "Occhialini", "Deniselivon", "Camacho", "Interamnia", "Kastel'", "Geflorsch", "Nurnberg", "Bienor", "GOI", "Lub", "Mitau", "Miyazawaseiroku", "Ballard", "Counselman", "Besixdouze", "Reunerta", "Akhilmathew", "Awaji", "Purple Mountain", "Enzomora", "Abigreene", "Menkaure", "Ursa", "Glo", "Peacock", "Okina-Ouna", "Dermott", "Kwak", "Akihiro", "Soderblom", "Elizkolod", "Strogen", "Brownlee", "Ouro Preto", "Bechstein", "Beletskij", "Tinkaping", "Dommanget", "Wallach", "Beatrix", "Glasenappia", "Lamarck", "Pathak", "Zoser", "McArthur", "Erikdeul", "Hoshinohiroba", "Fauvaud", "Tournesol", "Dabu", "Michaelreynolds", "Prabakaran", "Ekladyous", "Lamb", "Golovanov", "Clarkben", "Hubei", "Hveen", "Nissen", "Ebright", "Robertovittori", "Gywilliams", "Devonburr", "Aratus", "Shichangxu", "Rhaeticus", "Takaishuji", "Hergiani", "Arthurkuan", "Vienna" }

local function populateWorld(w)
  -- player
  w:setPlayer(Player.new())

  -- ship type
  local image = love.graphics.newImage("assets/Space shooter/playerShip1_blue.png")
  local shipType = ShipType.new(image, math.pi/2)
  shipType.acceleration    = 10
  shipType.angularVelocity = 2
  shipType.maxVelocity     = 20

  -- star systems
  local numberOfStarSystems = random(20, 30)
  for i = 1, numberOfStarSystems do
    local x = random(0, 500)
    local y = random(0, 500)
    local name = names[math.random(#names)]
    local starSystem = StarSystem.new(x, y, name)
    w:addStarSystem(starSystem)

    -- planets
    local numberOfPlanets = random(1, 6)
    for i = 1, numberOfPlanets do
      local num = random(0, 17)
      local img = love.graphics.newImage("assets/planets/" .. tostring(num) .. ".png")
      local x = random(-5000, 5000)
      local y = random(-5000, 5000)
      starSystem:addPlanet(Planet.new(x, y, img))
    end
  end

  -- ship
  local startingStarSystem = w.starSystems[1]
  w:setShip(Ship.new(20, 40, shipType, startingStarSystem))
end

function World.new()
  local t = {
    ship         = nil,
    starSystems  = {},
  }

  local world = setmetatable(t, World)

  populateWorld(world)

  return world
end

function World:setPlayer(player)
  self.player = player
end

function World:setShip(ship)
  self.ship = ship
end

function World:addStarSystem(starSystem)
  table.insert(self.starSystems, starSystem)
end

function World:getRelevantEntities()
  local entities = Set.new()

  entities:add(self.ship)

  local starSystem = self.ship.locationComponent.starSystem
  for _, planet in ipairs(starSystem.planets) do
    entities:add(planet)
  end

  return entities
end

return World
