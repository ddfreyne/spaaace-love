local Map = {}
Map.__index = Map

local Colors    = require('renderers/colors')
local Gamestate = require('vendor/hump/gamestate')

local stateName = 'map'

local function drawingOffset(world)
  local left, right
  local top, bottom
  for _, starSystem in ipairs(world.starSystems) do
    local position = starSystem.transformComponent.position

    if not left   or position.x < left   then left   = position.x end
    if not right  or position.x > right  then right  = position.x end
    if not bottom or position.y > bottom then bottom = position.y end
    if not top    or position.y < top    then top    = position.y end
  end

  local offsetX = love.window.getWidth() / 2 - (right - left) / 2
  local offsetY = love.window.getHeight() / 2 - (bottom - top) / 2

  return offsetX, offsetY
end

function Map.new(world)
  local buttonWidth = 400
  local buttonHeight = 60
  local yOffset = love.window.getHeight() - 100
  local xOffset = love.window.getWidth() / 2 - buttonWidth / 2

  local closeButton = loveframes.Create("button")
  closeButton:SetState(stateName)
  closeButton:SetPos(xOffset, yOffset)
  closeButton:SetWidth(buttonWidth)
  closeButton:SetHeight(buttonHeight)
  closeButton:SetText("Close")
  closeButton.OnClick = function(object)
    Gamestate.pop()
  end

  local t = {
    world       = world,
  }

  return setmetatable(t, Map)
end

function Map:enter()
  loveframes.SetState(stateName)
end

function Map:draw()
  local offsetX, offsetY = drawingOffset(self.world)

  love.graphics.push()
  love.graphics.translate(offsetX, offsetY)

  for _, starSystem in ipairs(self.world.starSystems) do
    local starSystemPosition = starSystem.transformComponent.position

    local currentStarSystem = self.world.ship.locationComponent.starSystem
    if starSystem == currentStarSystem then
      Colors.set(Colors.white)
    else
      Colors.set(Colors.cyan)
    end
    love.graphics.circle('fill', starSystemPosition.x, starSystemPosition.y, 3)

    if starSystem == self.world.ship.starSystemNavigationComponent.destination then
      Colors.set(Colors.white)
      love.graphics.circle('line', starSystemPosition.x, starSystemPosition.y, 6)
    end
  end

  love.graphics.pop()

  loveframes.draw()
end

function Map:update(delta)
  loveframes.update(delta)
end

function Map:mousepressed(x, y, button)
  loveframes.mousepressed(x, y, button)
end

function Map:mousereleased(x, y, button)
  loveframes.mousereleased(x, y, button)

  local offsetX, offsetY = drawingOffset(self.world)
  for _, starSystem in ipairs(self.world.starSystems) do
    local position = starSystem.transformComponent.position
    local starSystemPoint = Point.new(position.x + offsetX, position.y + offsetY)
    local clickPoint      = Point.new(x, y)

    if clickPoint:vectorTo(starSystemPoint):getLength() < 7 then
      self.world.ship.starSystemNavigationComponent.destination = starSystem
    end
  end
end

function Map:keypressed(key, unicode)
  loveframes.keypressed(key, unicode)
end

function Map:keyreleased(key)
  loveframes.keyreleased(key)
end

return Map
