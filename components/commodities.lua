local CommoditiesComponent = {}
CommoditiesComponent.__index = CommoditiesComponent

local function random(min, max)
  return min + math.floor(math.random() * (max - min))
end

function CommoditiesComponent.new()
  local t = {
    commodities = {
      food     = random(100, 1000),
      medicine = random(100, 1000),
      iron     = random(100, 1000),
      uranium  = random(100, 1000),
      drugs    = random(100, 1000),
    }
  }

  return setmetatable(t, CommoditiesComponent)
end

return CommoditiesComponent
