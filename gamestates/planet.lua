local Planet = {}
Planet.__index = Planet

local Gamestate        = require('vendor/hump/gamestate')
local PlanetTradeState = require('gamestates/planet-trade')
local Fonts            = require('renderers/fonts')
local Colors           = require('renderers/colors')

require('engine/types')

local stateName = 'planet'

local infoPanelSize = Size.new(1200, 600)
local windowSize    = Size.new(1200, 800)

local function getWindowOffset()
  return Point.new(
    love.window.getWidth()  / 2 - windowSize.width  / 2,
    love.window.getHeight() / 2 - windowSize.height / 2
  )
end

function Planet.new(world)
  local windowOffset = getWindowOffset()

  local buttonWidth = 400
  local buttonHeight = 60
  local buttonSpacing = 40

  local numButtons = 2
  local buttonBarWidth = numButtons * buttonWidth + (numButtons - 1) * buttonSpacing
  local leftmostButtonX = windowOffset.x + windowSize.width / 2 - buttonBarWidth / 2

  local tradeButton = loveframes.Create("button")
  tradeButton:SetState(stateName)
  tradeButton:SetPos(
    leftmostButtonX,
    windowOffset.y + windowSize.height - 100
  )
  tradeButton:SetWidth(buttonWidth)
  tradeButton:SetHeight(buttonHeight)
  tradeButton:SetText("Trade")
  tradeButton.OnClick = function(object)
    local planet = world.ship.locationComponent.planet
    local planetTradeState = PlanetTradeState.new(world, planet)
    Gamestate.push(planetTradeState)
  end

  local takeOffButton = loveframes.Create("button")
  takeOffButton:SetState(stateName)
  takeOffButton:SetPos(
    leftmostButtonX + buttonWidth + buttonSpacing,
    windowOffset.y + windowSize.height - 100
  )
  takeOffButton:SetWidth(buttonWidth)
  takeOffButton:SetHeight(buttonHeight)
  takeOffButton:SetText("Take off")
  takeOffButton.OnClick = function(object)
    world.ship.locationComponent.planet = nil
    Gamestate.pop()
  end

  local t = { world = world }

  return setmetatable(t, Planet)
end

local function drawBoxedText(string, font, x, bottomY, padding)
  -- Font
  love.graphics.setFont(font)
  local font = love.graphics.getFont()

  -- Coords
  local textWidth  = font:getWidth(string)
  local textHeight = font:getHeight()
  local boxWidth   = textWidth + padding * 2
  local boxHeight  = textHeight + padding * 2
  local y = bottomY - boxHeight

  -- Draw box
  love.graphics.setColor(0, 0, 0, 128)
  love.graphics.rectangle('fill', x, y, boxWidth, boxHeight)

  -- Draw text
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.print(string, x + padding, y + padding)

  -- Return size
  return Size.new(boxWidth, boxHeight)
end

function Planet:enter()
  loveframes.SetState(stateName)
end

function Planet:draw()
  -- TODO check presence of components
  local planet = self.world.ship.locationComponent.planet
  local planetKindComponent = planet.planetKindComponent
  local nameComponent = planet.nameComponent

  love.graphics.push()
  local windowOffset = getWindowOffset()
  love.graphics.translate(windowOffset.x, windowOffset.y)

  -- Draw image
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.draw(planetKindComponent.detailImage, 0, 0)

  -- Draw desc
  -- TODO generate random desc
  local desc = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
  local boxSize = drawBoxedText(
    desc,
    Fonts.body,
    20, planetKindComponent.detailImage:getHeight() - 20,
    20)

  -- Draw name
  drawBoxedText(
    nameComponent.name,
    Fonts.title,
    20, planetKindComponent.detailImage:getHeight() - 20 - boxSize.height,
    20)

  Colors.set(Colors.cyan)
  love.graphics.rectangle('line', -0.5, -0.5, windowSize.width, windowSize.height)

  love.graphics.pop()

  loveframes.draw()
end

function Planet:update(delta)
  loveframes.update(delta)
end

function Planet:mousepressed(x, y, button)
  loveframes.mousepressed(x, y, button)
end

function Planet:mousereleased(x, y, button)
  loveframes.mousereleased(x, y, button)
end

function Planet:keypressed(key, unicode)
  loveframes.keypressed(key, unicode)
end

function Planet:keyreleased(key)
  loveframes.keyreleased(key)
end

return Planet
