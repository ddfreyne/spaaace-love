require('engine/types')

local ShipType = {}
ShipType.__index = ShipType

function ShipType.new(image, imageRotation)
  local t = {
    image           = image,
    imageRotation   = imageRotation,
    acceleration    = 10,
    angularVelocity = 2,
    maxVelocity     = 20,
  }

  return setmetatable(t, ShipType)
end

return ShipType
