# Game architecture questions

## Can a game object belong to multiple spaces?

Some entities need to be drawn in multiple different views. For example, a spaceship’s sprite might be drawn in the middle of the screen, but the spaceship should also appear in the mini-map, where it is drawn differently.

The main gameplay view and the HUD (which contains the mini-map) are two different spaces.

Some observations:

* The rendering mechanism for the main gameplay view is entirely different from the rendering mechanism for the mini-map. This makes me think that the rendering systems (or rendering _subspaces_) should be different from the

* Whether or not a ship is drawn in the mini-map and/or the main gameplay view is unrelated. A ship could have a physical cloak, making it invisible in the main view, or it could have a radar cloak, making it invisible on the mini-map.

The answer to the question whether a game object can belong to multiple spaces seems to be **yes**.

## Which GUI framework is the best fit?

There are two choices for LÖVE:

* Quickie
* LoveFrames

Drawbacks of Quickie:

* Hard to implement hover/active states
* Hard to define the GUI in a declarative way

Drawbacks of LoveFrames:

* Does not integrate well with other gamestate libraries
* Hard to style
* Skins need to go into the LoveFrames directory

Current idea: use the game objects stuff I have written and create Button, Checkbox, entities along with a GUISystem. Advantages:

* I will need logic to select items not in the GUI anyway. This means having selected/hovering/… states.

## How can the renderer know how to render an entity?

There can be many kinds of visual components. For example:

* Sprite
* Procedurally rendered (e.g. starfield)
* Buttons
	- Rendered using vector graphics code
	- Using sliced textures
* Minimap
* Scrolling text for credits

Moreover, the same entity can have different representations in different game states or areas. For instance, a planet can be represented as an image in the main game view, or as a dot on the minimap.

Perhaps entities need a component that indicates its type, so that the renderer can decide how to represent it on the screen. Button entities could have ButtonComponents that are empty, for instance. Label entities could have a LabelComponent. Along with that, HoverableComponent and ClickableComponent that don't contain anything (although how would we know that a button is clicked? Also, what does it mean for a button not to have a clickable component?)

* * *

New idea: have multiple rendering systems that find the right entities and render them. This is how the MinimapRenderer currently works. A drawback might be that we run into ordering issues.

We should also start using components for tagging things, rather than using string tags.

## How can entities/components keep track of other entities?

On first thought, a minimap GUI entity will need to know the location of the player, ships and planets around it. But maybe only the rendering system needs to know about that.
