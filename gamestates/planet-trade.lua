local PlanetTrade = {}
PlanetTrade.__index = PlanetTrade

local Gamestate = require('vendor/hump/gamestate')
local Colors    = require('renderers/colors')
local Fonts     = require('renderers/fonts')

require('engine/types')

local stateName = 'planet-trade'

local titleOffset             = Point.new(20,  20 )
local nameOffset              = Point.new(20,  200)
local priceOffset             = Point.new(100, 200)
local amountOffset            = Point.new(150, 200)
local buyButtonOffset         = Point.new(250, 200)
local sellButtonOffset        = Point.new(350, 200)
local commodityButtonSize     = Size.new(80, 20)
local commodityButtonSpacing  = 30
local mainButtonSize          = Size.new(400, 60)
local mainButtonSpacing       = 40
local commodityButtonFont     = Fonts.body

-- TODO deduplicate
local windowSize    = Size.new(1200, 800)

local function getWindowOffset()
  return Point.new(
    love.window.getWidth()  / 2 - windowSize.width  / 2,
    love.window.getHeight() / 2 - windowSize.height / 2
  )
end

function PlanetTrade.new(world, planet)
  local commodityInventoryComponent = world.ship.commodityInventoryComponent
  local creditsComponent = world.player.creditsComponent
  local commoditiesComponent = planet.commoditiesComponent

  -- TODO handle these situations in a better way
  if not commodityInventoryComponent then return end
  if not creditsComponent            then return end
  if not commoditiesComponent        then return end

  local windowOffset = getWindowOffset()

  local numButtons = 2
  local buttonBarWidth = numButtons * mainButtonSize.width + (numButtons - 1) * mainButtonSpacing
  local leftmostButtonX = windowOffset.x + windowSize.width / 2 - buttonBarWidth / 2

  local closeButton = loveframes.Create("button")
  closeButton:SetState(stateName)
  closeButton:SetPos(
    leftmostButtonX + mainButtonSize.width + mainButtonSpacing,
    windowOffset.y + windowSize.height - 100)
  closeButton:SetWidth(mainButtonSize.width)
  closeButton:SetHeight(mainButtonSize.height)
  closeButton:SetText("Close")
  closeButton.OnClick = function(object)
    Gamestate.pop()
    Gamestate.current():enter() -- FIXME ugly hack
  end

  local i = 1
  for name, price in pairs(commoditiesComponent.commodities) do
    local buyButton = loveframes.Create("button")
    buyButton:SetState(stateName)
    buyButton:SetPos(
      windowOffset.x + buyButtonOffset.x,
      windowOffset.y + buyButtonOffset.y + commodityButtonSpacing * i)
    buyButton:SetWidth(commodityButtonSize.width)
    buyButton:SetHeight(commodityButtonSize.height)
    buyButton:SetText("Buy")
    buyButton.font = commodityButtonFont
    buyButton.OnClick = function(object)
      local amount = commodityInventoryComponent.amounts[name] or 0
      if price <= creditsComponent.amount then
        commodityInventoryComponent.amounts[name] = amount + 1
        creditsComponent.amount = creditsComponent.amount - price
      end
    end

    local sellButton = loveframes.Create("button")
    sellButton:SetState(stateName)
    sellButton:SetPos(
      windowOffset.x + sellButtonOffset.x,
      windowOffset.y + sellButtonOffset.y + commodityButtonSpacing * i)
    sellButton:SetWidth(commodityButtonSize.width)
    sellButton:SetHeight(commodityButtonSize.height)
    sellButton:SetText("Sell")
    sellButton.font = commodityButtonFont
    sellButton.OnClick = function(object)
      local amount = commodityInventoryComponent.amounts[name] or 0
      if amount > 0 then
        commodityInventoryComponent.amounts[name] = amount - 1
        creditsComponent.amount = creditsComponent.amount + price
      end
    end
    i = i + 1
  end

  local t = {
    world = world,
    planet = planet,
  }

  return setmetatable(t, PlanetTrade)
end

function PlanetTrade:enter()
  loveframes.SetState(stateName)
end

function PlanetTrade:draw()
  love.graphics.push()
  local windowOffset = getWindowOffset()
  love.graphics.translate(windowOffset.x, windowOffset.y)

  love.graphics.setFont(Fonts.head)
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.print("Trade", titleOffset.x, titleOffset.y)

  -- TODO check presence of components
  local planet = self.world.ship.locationComponent.planet
  local commoditiesComponent = planet.commoditiesComponent
  local commodityInventoryComponent = self.world.ship.commodityInventoryComponent
  local i = 1
  for name, price in pairs(commoditiesComponent.commodities) do
    local amount = commodityInventoryComponent.amounts[name] or 0

    love.graphics.setFont(Fonts.body)
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.print(name,   nameOffset.x,   nameOffset.y   + commodityButtonSpacing * i)
    love.graphics.print(price,  priceOffset.x,  priceOffset.y  + commodityButtonSpacing * i)
    love.graphics.print(amount, amountOffset.x, amountOffset.y + commodityButtonSpacing * i)

    i = i + 1
  end

  Colors.set(Colors.cyan)
  love.graphics.rectangle('line', -0.5, -0.5, windowSize.width, windowSize.height)

  love.graphics.pop()

  loveframes.draw()
end

function PlanetTrade:mousereleased(x, y, button)
  loveframes.mousereleased(x, y, button)
end

function PlanetTrade:update(delta)
  loveframes.update(delta)
end

function PlanetTrade:mousepressed(x, y, button)
  loveframes.mousepressed(x, y, button)
end

function PlanetTrade:keypressed(key, unicode)
  loveframes.keypressed(key, unicode)
end

function PlanetTrade:keyreleased(key)
  loveframes.keyreleased(key)
end

return PlanetTrade
