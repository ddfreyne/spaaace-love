local RenderingSystem = {}
RenderingSystem.__index = RenderingSystem

local function sortedPairs(t, order)
  local keys = {}
  for k in t:pairs() do
    keys[#keys+1] = k
  end

  if order then
    table.sort(keys, function (a, b) return order(t, a, b) end)
  else
    table.sort(keys)
  end

  local i = 0
  return function()
    i = i + 1
    if keys[i] then
      return keys[i], t[keys[i]]
    end
  end
end

local sortFn = function(t, ka, kb)
  local ap = ka.transformComponent
  local bp = kb.transformComponent

  if not ap or not bp then
    return true
  end

  return ap.z < bp.z
end

local function centerOnPointWhile(point, fn)
  love.graphics.push()

  local dx = love.window.getWidth()  / 2 - point.x
  local dy = love.window.getHeight() / 2 - point.y
  love.graphics.translate(dx, dy)
  fn()

  love.graphics.pop()
end

function RenderingSystem.new(entitiesCollection)
  return setmetatable({ entitiesCollection = entitiesCollection }, RenderingSystem)
end

function RenderingSystem:update(dt)
  local camera = self.entitiesCollection:firstWithTag('camera')
  if not camera then return end
  if not camera.transformComponent then return end

  centerOnPointWhile(camera.transformComponent.position, function()
    for entity in sortedPairs(self.entitiesCollection, sortFn) do
      self:updateEntity(entity, camera.transformComponent)
    end
  end)
end

function RenderingSystem:updateEntity(entity, cameraTransformComponent)
  local renderingComponent = entity.renderingComponent
  if not renderingComponent then return end

  local transformComponent = entity.transformComponent
  if not transformComponent then return end

  love.graphics.push()

  love.graphics.translate(transformComponent.position.x, transformComponent.position.y)
  love.graphics.rotate(transformComponent.angle)

  -- TODO render in correct order (lower z, entity itself, higher z)
  for childEntity in sortedPairs(entity.children or Set.new(), sortFn) do
    self:updateEntity(childEntity, cameraTransformComponent)
  end

  entity.renderingComponent:render(entity)

  love.graphics.pop()
end

return RenderingSystem
