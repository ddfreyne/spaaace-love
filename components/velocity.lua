require('engine/types')

local VelocityComponent = {}
VelocityComponent.__index = VelocityComponent

function VelocityComponent.new(x, y, maxVelocity, maxAngularVelocity)
  local t = {
    vel                = Vector.new(x, y),
    maxVelocity        = maxVelocity,
    maxAngularVelocity = maxAngularVelocity,
  }

  return setmetatable(t, VelocityComponent)
end

return VelocityComponent
