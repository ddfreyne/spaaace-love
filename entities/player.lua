local Entity = require('entities/entity')
local CreditsComponent = require('components/credits')

local Player = {}
Player.__index = Player

function Player.new(x, y, image)
  local entity = Entity.new()

  entity:setTag('player')

  entity.creditsComponent = CreditsComponent.new(2000)

  return entity
end

return Player
