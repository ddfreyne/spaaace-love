local Colors = {}
Colors.__index = Colors

Colors.white          = { 255, 255, 255, 255 }
Colors.gray           = { 128, 176, 192, 255 }
Colors.black          = { 0,   0,   0,   255 }

Colors.cyan           = { 0,   191, 255, 255 }
Colors.semiBrightCyan = { 0,   96,  127, 255 }
Colors.semiDarkCyan   = { 0,   48,  63,  255 }
Colors.darkCyan       = { 0,   23,  31,  255 }

Colors.orange         = { 255, 192, 0,   255 }

Colors.darkRed        = { 48,  18,  18,  255 }
Colors.red            = { 255, 64,  64,  255 }

function Colors.set(color)
  love.graphics.setColor(color[1], color[2], color[3], color[4])
end

return Colors
