require('engine/types')

local CursorTrackingComponent = {}
CursorTrackingComponent.__index = CursorTrackingComponent

function CursorTrackingComponent.new()
  local t = {
    isCursorInside = false,
  }

  return setmetatable(t, CursorTrackingComponent)
end

return CursorTrackingComponent
