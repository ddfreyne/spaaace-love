-- vector

Vector = {}
Vector.__index = Vector

function Vector.new(x, y)
  return setmetatable({ x = x, y = y }, Vector)
end

function Vector:dup()
  return Vector.new(self.x, self.y)
end

function Vector:invertX()
  self.x = - self.x
end

function Vector:invertY()
  self.y = - self.y
end

function Vector:limitToLength(maxLength)
  local length = self:getLength()
  if length > maxLength then
    local factor = maxLength / length
    self.x = self.x * factor
    self.y = self.y * factor
  end
end

function Vector:div(num)
  return Vector.new(self.x / num, self.y / num)
end

function Vector:asPoint()
  return Point.new(self.x, self.y)
end

function Vector:getLength()
  return math.sqrt(math.pow(self.x, 2) + math.pow(self.y, 2))
end

function Vector:angle()
  return math.atan2(self.y, self.x)
end

function Vector:cos()
  return math.cos(self:angle())
end

function Vector:sin()
  return math.sin(self:angle())
end

-- angle

Angle = {}

function Angle.normalize(rad)
  if rad < -math.pi then
    return Angle.normalize(rad + 2 * math.pi)
  elseif rad > math.pi then
    return Angle.normalize(rad - 2 * math.pi)
  else
    return rad
  end
end

-- set

Set = {}
Set.__index = Set

function Set.new()
  return setmetatable({ vals = {} }, Set)
end

function Set:add(e)
  self.vals[e] = true
end

function Set:remove(e)
  self.vals[e] = false
end

function Set:each()
  return pairs(self.vals)
end

function Set:pairs()
  return pairs(self.vals)
end

-- point

Point = {}
Point.__index = Point

function Point.new(x, y)
  return setmetatable({ x = x, y = y }, Point)
end

function Point:dup()
  return Point.new(self.x, self.y)
end

function Point:asVector()
  return Vector.new(self.x, self.y)
end

function Point:vectorTo(other)
  return Vector.new(other.x - self.x, other.y - self.y)
end

-- size

Size = {}
Size.__index = Size

function Size.new(width, height)
  return setmetatable({ width = width, height = height }, Size)
end

function Size:dup()
  return Size.new(self.width, self.height)
end

function Size:xMiddle()
  return self.width/2
end

function Size:yMiddle()
  return self.height/2
end

-- range

Range = {}
Range.__index = Range

function Range.new(min, max)
  return setmetatable({ min = min, max = max }, Range)
end

function Range:includesValue(value)
  return value > self.min and value < self.max
end

function Range:overlapsWith(other)
  return self:includesValue(other.min) or other:includesValue(self.min)
end

-- rect

Rect = {}
Rect.__index = Rect

function Rect.new(x, y, width, height)
  return setmetatable({ origin = Point.new(x, y), size = Size.new(width, height) }, Rect)
end

function Rect:collidesWith(other)
  return self:xRange():overlapsWith(other:xRange()) and self:yRange():overlapsWith(other:yRange())
end

function Rect:xRange()
  return Range.new(self:left(), self:right())
end

function Rect:yRange()
  return Range.new(self:top(), self:bottom())
end

function Rect:middle()
  return self:xMiddle(), self:yMiddle()
end

function Rect:xMiddle()
  return self.origin.x + self.size.width/2
end

function Rect:yMiddle()
  return self.origin.y + self.size.height/2
end

function Rect:left()
  return self.origin.x
end

function Rect:right()
  return self.origin.x + self.size.width
end

function Rect:top()
  return self.origin.y
end

function Rect:bottom()
  return self.origin.y + self.size.height
end

function Rect:isFullyBelow(other)
  return self:top() > other:bottom()
end

function Rect:isFullyAbove(other)
  return self:bottom() < other:top()
end

function Rect:isFullyLeft(other)
  return self:right() < other:left()
end

function Rect:isFullyRight(other)
  return self:left() > other:right()
end

function Rect:containsPoint(point)
  return self:xRange():includesValue(point.x) and self:yRange():includesValue(point.y)
end

function Rect:fill()
  love.graphics.rectangle(
    "fill",
    self.origin.x,
    self.origin.y,
    self.size.width,
    self.size.height)
end

-- entities collection

EntitiesCollection = {}
EntitiesCollection.__index = EntitiesCollection

function EntitiesCollection.new()
  local t = {
    entities = Set.new(),
    byTag    = {},
  }

  return setmetatable(t, EntitiesCollection)
end

function EntitiesCollection:replaceEntities(entities)
  self.entities = Set.new()
  for entity in entities:pairs() do
    self:add(entity)
  end
end

function EntitiesCollection:add(entity)
  self.entities:add(entity)

  if entity.tag and not self.byTag[entity.tag] then
    self.byTag[entity.tag] = entity
  end
end

function EntitiesCollection:remove(entity)
  self.entities:remove(entity)

  if entity.tag and self.byTag[entity.tag] == entity then
    self.byTag[entity.tag] = nil
  end
end

function EntitiesCollection:pairs()
  return self.entities:pairs()
end

function EntitiesCollection:firstWithTag(tag)
  return self.byTag[tag]
end
