local StarSystemNavigationComponent = {}
StarSystemNavigationComponent.__index = StarSystemNavigationComponent

function StarSystemNavigationComponent.new()
  local t = {
    destination = nil,
  }

  return setmetatable(t, StarSystemNavigationComponent)
end

return StarSystemNavigationComponent
