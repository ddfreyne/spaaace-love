local AnimationSystem = {}
AnimationSystem.__index = AnimationSystem

function AnimationSystem.new(entitiesCollection)
  return setmetatable({ entitiesCollection = entitiesCollection }, AnimationSystem)
end

function AnimationSystem:update(dt)
  for entity in self.entitiesCollection:pairs() do
    self:updateEntity(entity, dt)
  end
end

function AnimationSystem:updateEntity(entity, dt)
  local renderingComponent = entity.renderingComponent
  if not renderingComponent then return end

  if renderingComponent.progress then
    renderingComponent.progress = renderingComponent.progress + dt
  end

  for childEntity in (entity.children or Set.new()):pairs() do
    self:updateEntity(childEntity, dt)
  end
end

return AnimationSystem
