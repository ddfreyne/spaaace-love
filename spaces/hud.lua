local Entity                  = require('entities/entity')
local CursorTrackingComponent = require('components/cursor_tracking')
local TransformComponent      = require('components/transform')
local SizeComponent           = require('components/size')
local RenderingSystem         = require('systems/rendering')
local CursorTrackingSystem    = require('systems/cursor_tracking')
local Colors                  = require('renderers/colors')
local Fonts                   = require('renderers/fonts')
local Camera                  = require('entities/camera')

require('engine/types')




local BoxRenderingComponent = {}
BoxRenderingComponent.__index = BoxRenderingComponent

function BoxRenderingComponent.new()
  return setmetatable({}, BoxRenderingComponent)
end

function BoxRenderingComponent:render(entity)
  local sizeComponent = entity.sizeComponent
  if not sizeComponent then return end

  love.graphics.push()
  love.graphics.translate(-sizeComponent.size.width / 2, -sizeComponent.size.height / 2)

  if entity.cursorTrackingComponent and entity.cursorTrackingComponent.isCursorInside then
    Colors.set(Colors.semiBrightCyan)
  else
    Colors.set(Colors.semiDarkCyan)
  end
  love.graphics.rectangle('fill', 0, 0, sizeComponent.size.width, sizeComponent.size.height)

  if entity.cursorTrackingComponent and entity.cursorTrackingComponent.isCursorInside and love.mouse.isDown('l') then
    Colors.set(Colors.white)
  else
    Colors.set(Colors.semiBrightCyan)
  end
  love.graphics.rectangle('line', 0, 0, sizeComponent.size.width, sizeComponent.size.height)

  Colors.set(Colors.white)
  local content = '' .. entity.textFn()
  local font
  if #content > 12 then font = Fonts.hudBoxContentSmall else font = Fonts.hudBoxContentBig end
  love.graphics.setFont(font)
  love.graphics.printf(entity.textFn(), 0, 0 + (sizeComponent.size.height - font:getHeight())/2, sizeComponent.size.width, 'center')

  Colors.set(Colors.white)
  love.graphics.setFont(Fonts.hudBoxLabel)
  love.graphics.printf(entity.labelText, 0, 0 + sizeComponent.size.height, sizeComponent.size.width, 'center')

  love.graphics.pop()
end





local Box = {}
Box.__index = Box

function Box.new(x, y, w, h, labelText, textFn)
  -- TODO convert into proper entity

  local t = {
    transformComponent      = TransformComponent.new(x, y, 0, 0),
    renderingComponent      = BoxRenderingComponent.new(),
    sizeComponent           = SizeComponent.new(Size.new(w, h)),
    cursorTrackingComponent = CursorTrackingComponent.new(),
    labelText = labelText,
    textFn    = textFn,
  }

  return setmetatable(t, Box)
end





local MinimapRenderingSystem = {}
MinimapRenderingSystem.__index = MinimapRenderingSystem

function MinimapRenderingSystem.new(hudEntitiesCollection, arenaEntitiesCollection)
  local t = {
    hudEntitiesCollection   = hudEntitiesCollection,
    arenaEntitiesCollection = arenaEntitiesCollection,
  }

  return setmetatable(t, MinimapRenderingSystem)
end

function MinimapRenderingSystem:update(dt)
  local minimap = self.hudEntitiesCollection:firstWithTag('minimap')
  if not minimap then return end

  local transformComponent = minimap.transformComponent
  if not transformComponent then return end

  love.graphics.push()
  love.graphics.translate(transformComponent.position.x, transformComponent.position.y)

  -- Background
  love.graphics.setLineWidth(3)
  Colors.set(Colors.darkCyan)
  love.graphics.circle('fill', 0, 0, minimap.minimapDiameter / 2)
  Colors.set(Colors.cyan)
  love.graphics.circle('line', 0, 0, minimap.minimapDiameter / 2)

  -- Inner circles
  love.graphics.setLineWidth(1)
  Colors.set(Colors.semiDarkCyan)
  love.graphics.circle('line', 0, 0, minimap.minimapDiameter / 2 * 0.75)
  love.graphics.circle('line', 0, 0, minimap.minimapDiameter / 2 * 0.50)
  love.graphics.circle('line', 0, 0, minimap.minimapDiameter / 2 * 0.25)

  -- Get central position
  local camera = self.arenaEntitiesCollection:firstWithTag('camera')
  if not camera then return end
  local cameraTransformComponent = camera.transformComponent
  if not cameraTransformComponent then return end

  -- Update
  for entity in self.arenaEntitiesCollection:pairs() do
    self:updateEntity(entity, cameraTransformComponent.position, minimap)
  end

  love.graphics.pop()
end

function MinimapRenderingSystem:updateEntity(entity, centerPos, minimap)
  local minimapRepresentationComponent = entity.minimapRepresentationComponent
  local transformComponent             = entity.transformComponent

  if not minimapRepresentationComponent then return end
  if not transformComponent             then return end

  local point = Point.new(
    (transformComponent.position.x - centerPos.x) / minimap.minimapScale,
    (transformComponent.position.y - centerPos.y) / minimap.minimapScale)

  -- FIXME ugly
  if entity.getTag and entity:getTag() == 'player_ship' then
    Colors.set(Colors.white)
  else
    Colors.set(Colors.cyan)
  end

  if point:asVector():getLength() < minimap.minimapDiameter / 2 then
    love.graphics.circle('fill', point.x, point.y, minimapRepresentationComponent.diameter / 2)
  end
end






local Minimap = {}
Minimap.__index = Minimap

function Minimap.new(diameter, offset, scale)
  local x = love.window.getWidth() - diameter / 2 - offset
  local y = offset + diameter / 2

  local entity = Entity.new()

  entity.transformComponent = TransformComponent.new(x, y, 0, 0)

  -- TODO move these into components
  entity.minimapDiameter = diameter
  entity.minimapScale    = scale

  entity:setTag('minimap')

  return setmetatable(entity, Minimap)
end







local HUD = {}
HUD.__index = HUD

function HUD.new(world, arenaSpace)
  local minimapDiameter = 200
  local minimapOffset   = 20
  local minimapScale    = 100

  local sidebarPadding        = 20
  local minimapSidebarSpacing = 20

  local w = minimapDiameter - 2 * sidebarPadding
  local h = 60
  local x = love.window.getWidth() - minimapDiameter / 2 - minimapOffset
  local y = minimapDiameter + minimapOffset * 2 + minimapSidebarSpacing + h / 2
  local s = 110

  local function credFn()
    return world.player.creditsComponent.amount
  end

  local function currFn()
    return world.ship.locationComponent.starSystem.nameComponent.name
  end

  local function targFn()
    local s = world.ship.starSystemNavigationComponent.destination
    if s then return s.nameComponent.name else return '--' end
  end

  local camera = Camera.new(
    love.window.getWidth() / 2,
    love.window.getHeight() / 2)

  local entitiesCollection = EntitiesCollection.new()
  entitiesCollection:add(Box.new(x, y + 0 * s, w, h, 'CREDITS', credFn))
  entitiesCollection:add(Box.new(x, y + 1 * s, w, h, 'CURRENT', currFn))
  entitiesCollection:add(Box.new(x, y + 2 * s, w, h, 'TARGET', targFn))
  entitiesCollection:add(Minimap.new(minimapDiameter, minimapOffset, minimapScale))
  entitiesCollection:add(camera)

  local t = {
    renderSystem            = RenderingSystem.new(entitiesCollection),
    minimapRenderingSystem  = MinimapRenderingSystem.new(entitiesCollection, arenaSpace.entitiesCollection),
    cursorTrackingSystem    = CursorTrackingSystem.new(entitiesCollection),
  }

  return setmetatable(t, HUD)
end

function HUD:update(dt)
  self.cursorTrackingSystem:update(dt)
end

function HUD:draw()
  self.renderSystem:update(0)
  self.minimapRenderingSystem:update(0)
end

function HUD:keypressed(key)
end

return HUD
