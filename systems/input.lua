local MapState           = require('gamestates/map')
local PlanetState        = require('gamestates/planet')
local Gamestate          = require('vendor/hump/gamestate')
local HyperjumpComponent = require('components/hyperjump')

local InputSystem = {}
InputSystem.__index = InputSystem

function InputSystem.new(world, entitiesCollection)
  return setmetatable({ world = world, entitiesCollection = entitiesCollection, key = nil }, InputSystem)
end

function InputSystem:handleHyperjumpRequest(ship)
  if not ship.starSystemNavigationComponent.destination then
    return
  end

  ship.hyperjumpComponent = HyperjumpComponent.new()
end

function InputSystem:handleLandRequest(ship)
  local locationComponent      = ship.locationComponent
  local shipTransformComponent = ship.transformComponent
  local velocityComponent      = ship.velocityComponent

  if not locationComponent      then return end
  if not shipTransformComponent then return end
  if not velocityComponent      then return end

  -- Find closest planet
  local closestPlanet   = nil
  local closestDistance = nil
  for _, planet in ipairs(locationComponent.starSystem.planets) do
    local planetTransformComponent = planet.transformComponent

    if planetTransformComponent then
      local from = shipTransformComponent.position
      local to   = planetTransformComponent.position
      local distance = from:vectorTo(to):getLength()

      if not closestDistance or closestDistance > distance then
        closestDistance = distance
        closestPlanet   = planet
      end
    end
  end

  -- Check distance
  local maximumDistance = 200
  if closestDistance > maximumDistance then return end

  -- Check speed
  local velocityComponent = ship.velocityComponent
  local maximumVelocity = 3
  if velocityComponent.vel:getLength() > maximumVelocity then
    local logComponent = ship.logComponent
    if logComponent then
      logComponent:add("Landing request denied: maximum velocity exceeded")
    end
    return
  end

  -- Set location and velocity
  shipTransformComponent.position.x = closestPlanet.transformComponent.position.x
  shipTransformComponent.position.y = closestPlanet.transformComponent.position.y
  velocityComponent.vel.x = 0
  velocityComponent.vel.y = 0

  -- Land
  locationComponent.planet = closestPlanet

  -- Change state
  local planetState = PlanetState.new(self.world)
  Gamestate.push(planetState)
end

function InputSystem:keypressed(key)
  self.key = key
end

function InputSystem:update(delta)
  local ship = self.entitiesCollection:firstWithTag('player_ship')
  if not ship then return end

  -- TODO this can probably be handled in a nicer way
  if ship.hyperjumpComponent then return end

  if self.key == "m" then
    mapState = MapState.new(self.world)
    Gamestate.push(mapState)
  elseif self.key == "j" then
    self:handleHyperjumpRequest(ship)
  elseif self.key == "l" then
    self:handleLandRequest(ship)
  end
  self.key = nil

  ship.accelerationComponent.isAccelerating = love.keyboard.isDown("up")
  ship.steeringComponent.isTurningLeft      = love.keyboard.isDown("left")
  ship.steeringComponent.isTurningRight     = love.keyboard.isDown("right")
  ship.steeringComponent.isReversing        = love.keyboard.isDown("down")
end

return InputSystem
